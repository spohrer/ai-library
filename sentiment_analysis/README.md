# Sentiment Analysis 

Sentiment Analysis refers to the use of several techniques to systematically identify, extract, quantify, and study emotional states and subjective information. It aims to determine the attitudes, opinions, or emotional reactions of a speaker with respect to some topic. It can often be helpful in ascertaining the sentiment of a product or brand when given conversation data such as a social media feed.

# Content

`sentimentproxy.py`  - Code takes in text and processes the sentiment using pre-trained model with tensorflow model server.

##### Parameters
* text - Text to process sentiment analysis on.

# Example

curl -v http://sentiment-analysis-ai-library.10.16.208.2.nip.io/api/v0.1/predictions -d '{"strData":"I like ice cream"}' -H "Content-Type: application/json"

# Result

{
  "meta": {
    "puid": "i63g2kkhehoifa982v34pu97d4",
    "tags": {
    },
    "routing": {
    },
    "requestPath": {
      "tfserving-model-en-proxy": "quay.io/opendatahub/ai-library-sentiment-analysis-proxy"
    },
    "metrics": []
  },
  "strData": "Positive"
* Connection #0 to host sentiment-analysis-ai-library.10.16.208.2.nip.io left intact
}
